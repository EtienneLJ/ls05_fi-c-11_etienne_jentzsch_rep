package konsolenausgabe;

public class Aufgabe2 {

	public static void main(String[] args) {
		String s = "*************";    // bestimmt wieviele Zeichen ich benutzen m�chte
		System.out.println("     *");
		System.out.println("    ***");
		System.out.println("   *****");
		System.out.println("  *******");
		System.out.println(" *********");
		System.out.println("***********");
		System.out.println("    ***");
		System.out.println("    ***");        //einfache Printausgabe mit Leertaste
		
		System.out.printf( "%10.1s\n", s );  //%10 zeigt die Zeilen an die ich benutze
		System.out.printf( "%11.3s\n", s ); //.3s zeigt wieviele Zeichen ich benutzen m�chte
		System.out.printf( "%12.5s\n", s ); // \n macht einen Zeichenvorschub
		System.out.printf( "%13.7s\n", s );
		System.out.printf( "%14.9s\n", s );
		System.out.printf( "%15.11s\n", s );
		System.out.printf( "%11.3s\n", s );
		System.out.printf( "%11.3s\n", s );	//printf komplizierter f�r kleine Bilder aber viel sch�ner f�r gr��ere Konstrukte
	}

}
