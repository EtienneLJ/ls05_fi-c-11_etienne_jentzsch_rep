package konsolenausgabe;

public class Aufgabe3 {

	public static void main(String[] args) {
		System.out.printf("Gleitkomma-%s!%n", "Zahlen"); // testversion
		double d = 22.4234234; // -> 22,42
		double x = 111.2222; // -> 111,22
		double a = 4.0; // -> 4,00
		double b = 1000000.551;  // -> 1000000,55
		double c = 97.34; // -> 97,34
		
		System.out.printf( "%.2f\n" , d); 
		System.out.printf( "%.2f\n" , x);
		System.out.printf( "%.2f\n" , a); 
		System.out.printf( "%.2f\n" , b);
		System.out.printf( "%.2f\n" , c);
				
		// %10.2f (10 zeigt die Stellen insgesamt an und die 2 zwei Nachkommastellen)
	}

}
