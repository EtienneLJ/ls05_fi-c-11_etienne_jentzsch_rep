package konsolenausgabe;

public class Aufgabe1 {

	public static void main(String[] args) {
		System.out.println("Das ist ein \"Beispielsatz!\"");
		System.out.println("Ein Beispielsatz ist das!");
		// dies ist ein Test f�r ein Kommentar
		int alter = 29;
		String name = "Etienne L. Jentzsch";
		System.out.println("Meine Name ist " + name + " und ich bin " + alter + " Jahre alt.");
		
		//print() benutzt keine Abs�tze und kettet alles aneinander:
		//Das ist ein "Beispielsatz!"Ein Beispielsatz ist das!Meine Name ist Etienne L. Jentzsch und ich bin 29 Jahre alt.
		
		//println() f�gt Abs�tze hinzu: 
		//Das ist ein "Beispielsatz!"
		//Ein Beispielsatz ist das!
		//Meine Name ist Etienne L. Jentzsch und ich bin 29 Jahre alt.
	}

}
