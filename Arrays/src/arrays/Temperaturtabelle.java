package arrays;

public class Temperaturtabelle {

	public static void main(String[] args) {
	       double[][] temperaturtabelle = neueTabelle(12);
	       tabelleAusgeben(temperaturtabelle);
		}
		
		public static double[][] neueTabelle(int reihe) {
			double[][] temperaturtabelle = new double[reihe][2]; //Auflistung der Berechnungen die in 2 Elementen gespeichert werden
			for (int i = 0; i < temperaturtabelle.length; i++) { //Listet solange auf bis der Anker 12 erreicht ist in der main
				temperaturtabelle[i][0] = (i+1) * 10.0; // Fahrenheitseite
				temperaturtabelle[i][1] = (5.0/9.0) * (temperaturtabelle[i][0] - 32); //Celsius Seite (Umrechnung von Fahrenheit F nach Celsius C: C = (5/9) � (F � 32))
			}
			return temperaturtabelle;
		}
		
		public static void tabelleAusgeben(double[][] ausgabeTabelle) {	
			System.out.println(   "    Farenheit   ||    Celsius    ");
			System.out.println(   "-------------------------------");
			for (int i = 0; i < ausgabeTabelle.length; i++) {
				System.out.format("      %4.1f           %4.1f    %n",ausgabeTabelle[i][0], ausgabeTabelle[i][1]); // Systemout.format kann alles gleichzeitig ausgegebn
			}
		}
}

//Schreiben Sie eine Methode, die eine Tabelle mit Temperaturwerten in ein 
//zweidimensionales Feld ablegt. In einer Zeile der Tabelle sollen zwei Werte abgelegt 
//werden: ein Wert in Fahrenheit mit dem entsprechenden umgerechneten Wert in Celsius. 
//Dabei soll in dem ersten Wert in Fahrenheit der Wert 0.0 sein, der zweite Wert in Fahrenheit 
//der Wert 10.0, ... Der Funktion soll die Anzahl der Zeilen, d.h. die Anzahl der zu 
//berechnenden Wertepaare als Parameter �bergeben werden. (Umrechnung von Fahrenheit 
//F nach Celsius C: C = (5/9) � (F � 32))
