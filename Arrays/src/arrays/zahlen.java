package arrays; // Etienne Jentzsch

public class zahlen {

	public static void main(String[] args) {
		int zahlenreihe [] = new int [10]; // int zahlen [] erstellt ein array, new int[10] belegt das array mit 10 Elementen
		for(int i = 0; i < 10; i++) {	// Zahlen 0 bis 9 eintragen (0-9 = 10 Elemente)
			zahlenreihe [i] = i;	// bef�llt das array
		}	
		for(int i = 0; i < zahlenreihe.length; i++) {	// Konsolenausgabe
			System.out.println(zahlenreihe[i]); // jede einzelne Zahl als Ausgabe
		}
	}
}

