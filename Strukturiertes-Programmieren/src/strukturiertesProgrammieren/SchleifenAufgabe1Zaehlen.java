package strukturiertesProgrammieren;
import java.util.Scanner;

public class SchleifenAufgabe1Zaehlen {

		public static void main(String[] args) {
			int a;		// benutzte Variable 
			Scanner myScanner = new Scanner(System.in);
			System.out.print("Bitte geben Sie eine natuerliche Zahl ein: ");  // Nur natürliche Zahlen sind erlaubt
			a = myScanner.nextInt(); // Variable in den Scanner überführen

			System.out.println("Hochgezaehlt:"); // Ausgabe vom Hochzaehlen
			hochzaehlen(a);

			System.out.println("\nHeruntergezaehlt:"); // Ausgabe vom Herunterzaehlen
			herunterzaehlen(a);
			myScanner.close(); // Scanner schließen
		}

		public static void hochzaehlen(int a) { // Alle Zahlen ausgeben von 1 bis zur eingebenen Zahl
			for (int i = 1; i <= a; i++) { 
				System.out.println(i);
			}
		}

		public static void herunterzaehlen(int a) { // Alle Zahlen ausgeben von der eingebenen Zahl
			for (int i = a; i > 0; i--) {
				System.out.println(i);
			}
		}
	}