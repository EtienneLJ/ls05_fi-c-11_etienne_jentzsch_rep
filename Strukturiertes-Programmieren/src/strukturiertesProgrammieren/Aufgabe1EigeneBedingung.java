package strukturiertesProgrammieren;
import java.util.Scanner;

public class Aufgabe1EigeneBedingung {
		
	public static void main(String[] args) {
		programmhinweis();
		Scanner MyScanner = new Scanner(System.in); 
		System.out.println("Gebe die 1.Zahl ein: ");
		double a = MyScanner.nextDouble();
		System.out.println("Gebe die 2.Zahl ein: ");
		double b = MyScanner.nextDouble();	
		gleich(a,b);
	}
	
    public static void programmhinweis(){
        System.out.println("Hinweis: ");
        System.out.println("Das Programm vergleicht 2 eingegebene Zahlen. ");
    }
    
    
	public static void gleich(double a, double b) {
			if (a == b ) {
				System.out.println("Die Zahlen sind gleich");
			}
			if (a<b) {
				System.out.println("Die zweite Zahl ist Gr��er");
			}
			else if (a>=b) {
				System.out.println("Die erste Zahl ist gleich oder gr��er als die zweite Zahl");
			}
	}
}