package strukturiertesProgrammieren;
import java.util.Scanner;

public class Schleifen2Aufgabe1Zaehlen {

	public static void main(String[] args) {
			int a;
			Scanner myScanner = new Scanner(System.in);
			System.out.print("Bitte geben Sie eine natürliche Zahl ein: ");
			a = myScanner.nextInt();

			System.out.println("Hochzaehlen:"); // Ausgabe Hochzaehlen
			hochzaehlen(a);

			System.out.println("\nHerunterzaehlen:"); // Ausgabe Herunterzaehlen
			herunterzaehlen(a);
			
			myScanner.close(); // Scanner schließen
		}

		public static void hochzaehlen(int a) {
			int i = 1;
			while ( i <= a ) {
			System.out.println(i);
			i++;
			}
		}

		public static void herunterzaehlen(int a) {
			int i = a;
			while ( i > 0 ) {
				System.out.println(i);
				 i--;
			}
		}
	}