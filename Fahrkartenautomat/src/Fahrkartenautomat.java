﻿import java.util.Locale; // von Etienne Jentzsch
import java.util.Scanner;

class Fahrkartenautomat {
	static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) { // Main Methode
		while (true) {
		System.out.println("\nFahrkartenbestellvorgang:");
		int millisekunde = 1;
		warte(millisekunde);
		double zuZahlenderBetrag = fahrkartenbestellung_Erfassen(); // Fahrkartenbestellung Erfassen
		int zuZahlenderBetrag1 = (int) (zuZahlenderBetrag * 100); // Komma-Nachstellen
		zuZahlenderBetrag = zuZahlenderBetrag1;
		double eingezahlterGesamtbetrag = 0.0; // Fahrkarten Bezahlen
		double rückgabebetrag = fahrkarten_Bezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag); //		double rückgabebetrag = fahrkarten_Bezahlen(eingezahlterGesamtbetrag, anzahl_der_Tickets);
		
		fahrkarten_Ausgaben("\nFahrschein wird ausgegeben"); // Fahrschein Ausgabe

		millisekunde = 200; // Wartezeit
		warte(millisekunde);

		rueckgeld_Ausgaben(rückgabebetrag); // Rückgeld Ausgaben
		//tastatur.close(); // Scanner schliessen
		abschied(); // BEnd-Text
	}
		}
	// Fahrkartenbestellung Erfassen
	public static double fahrkartenbestellung_Erfassen() {
		double gewaehlteFahrkarte;
		double zwischensumme = 0;
		int anzahltickets = 0;
		while (true) { 									 // Solange die Bedingungen in der Schleife erfüllt springt er zu der Anzahl der Tickets
			System.out.println("\nWählen Sie Ihre Wunschfahrkarte für Berlin AB aus: "); //Benutzer Informationen
			System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
			System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
			System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
			System.out.println("Zum Bezahlen (9)");
			gewaehlteFahrkarte = tastatur.nextDouble(); //Eingabe des Nutzers
			if (gewaehlteFahrkarte == 1) {
				System.out.println("Ihre Wahl: " + (int)gewaehlteFahrkarte);//1.Fahrkarte
				gewaehlteFahrkarte = 2.90;
//				break;									//break stoppt den Vorgang wenn die Bedingung erfüllt ist und returnt den Wert
			} else if (gewaehlteFahrkarte == 2) {
				System.out.println("Ihre Wahl: " + (int)gewaehlteFahrkarte);//2.Fahrkarte
				gewaehlteFahrkarte = 8.60;
//				break;
			} else if (gewaehlteFahrkarte == 3) {
				System.out.println("Ihre Wahl: " + (int)gewaehlteFahrkarte);//3.Fahrkarte
				gewaehlteFahrkarte = 23.50;
//				break;
			}
			  else if (gewaehlteFahrkarte == 9) {		//9 Wählen um aus der Schleife rauszugehen
					return zwischensumme;
				}
			  else {									//Wenn die Bedingungen nicht erfüllt sind, springt zum Anfang zurück
				System.out.println("Ihre Wahl: " + gewaehlteFahrkarte);
				System.out.println(">>>>>Falsche Eingabe<<<<<");
				System.out.println("Ticket bitte neu wählen: ");
			}
			System.out.print("Anzahl der Tickets: ");
			anzahltickets = tastatur.nextInt();
			if  (anzahltickets >= 1 && anzahltickets <= 10) {
				zwischensumme = zwischensumme + (anzahltickets * gewaehlteFahrkarte);
				System.out.printf("\nZwischensumme: %.2f €\n", zwischensumme);
//				return gewaehlteFahrkarte;
			} else {
				System.out.println("Die Anzahl der tickets muss mind. 1 und max. 10 sein");
			}
		}
	}

	// Fahrkarten Bezahlen
	public static double fahrkarten_Bezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf(Locale.US, "Noch zu zahlen: %.2f %s \n",(zuZahlenderBetrag / 100 - eingezahlterGesamtbetrag  / 100), "Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingeworfeneMünze = eingeworfeneMünze * 100;
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		// Rückgeldberechnung und -Ausgabe
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rückgabebetrag;
	}
	// Fahrkarten Ausgaben
	public static void fahrkarten_Ausgaben(String fahrschein_Text) {
		System.out.println(fahrschein_Text);
	}
	// Wartezeit
	public static void warte(int millisekunde) {
		for (int i = 0; i < 25; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	// Rückgeld Ausgaben
	public static void rueckgeld_Ausgaben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rückgabebetrag / 100);
			System.out.println("wird in folgenden Münzen ausgezahlt:");
			
			int rückgabebetrag1 = (int) rückgabebetrag;
			rückgabebetrag = rückgabebetrag1;
			// Münze Ausgaben
			while (rückgabebetrag1 >= 200) {
				muenzeAusgeben(rückgabebetrag1, "EURO");
				rückgabebetrag1 -= 200;
			}
			while (rückgabebetrag1 >= 100) {
				muenzeAusgeben(rückgabebetrag1, "EURO");
				rückgabebetrag1 -= 100;
			}
			while (rückgabebetrag1 >= 50) {
				muenzeAusgeben(rückgabebetrag1, "CENT");
				rückgabebetrag1 -= 50;
			}
			while (rückgabebetrag1 >= 20) {
				muenzeAusgeben(rückgabebetrag1, "CENT");
				rückgabebetrag1 -= 20;
			}
			while (rückgabebetrag1 >= 10) {
				muenzeAusgeben(rückgabebetrag1, "CENT");
				rückgabebetrag1 -= 10;
			}
			while (rückgabebetrag1 >= 5) {
				muenzeAusgeben(rückgabebetrag1, "CENT");
				rückgabebetrag1 -= 5;
			}
			while (rückgabebetrag1 >= 2) {
				muenzeAusgeben(rückgabebetrag1, "CENT");
				rückgabebetrag1 -= 2;
			}
			while (rückgabebetrag1 >= 1) {
				muenzeAusgeben(rückgabebetrag1, "CENT");
				rückgabebetrag1 -= 1;
			}
		}
	}
	// Münze Ausgeben
	public static void muenzeAusgeben(int betrag, String einheit) {
		if (betrag >= 100) {
		System.out.printf("%d" + " %s", (int)betrag / 100, einheit); // neu test - muss weg vieleicht
		}
		else {
		System.out.println("\n"+ betrag + " " + einheit);
		}
	}
	// Benutzer Begrüßung
	public static void abschied() {
		System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
}