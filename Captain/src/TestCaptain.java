//import Captain;

public class TestCaptain {

	public static void main(String[] args) {
		// Erzeugen der Objekte
		Captain cap1 = new Captain("Jean Luc", "Picard", 2364, 4500.0);
		Captain cap2 = new Captain("Azetbur", "Gorkon", 2373, 12);
		Captain cap3 = new Captain("Leonardo", "Etienne", 2222, 10000.0);
		Captain cap4 = new Captain("Turtle", "Ninja", 2555, 1000.0);
		Captain cap5 = new Captain("Pan", "Peter", 2421, 2500.0);
		Captain cap6 = new Captain(); //parameterlos
		
		/*
		 * TODO: 9. Erzeugen Sie ein zusaetzliches Objekt cap3 und geben Sie es auch auf
		 * der Konsole aus, die Attributwerte denken Sie sich aus.
		 */
		/*
		 * TODO: 10. Erzeugen Sie zwei zusaetzliche Objekte cap4 und cap5 mit dem
		 * Konstruktor, der den Namen und Vornamen initialisiert, die Attributwerte
		 * denken Sie sich aus.
		 */

		// Setzen der Attribute
		/*
		 * TODO: 11. Fuegen Sie cap4 und cap5 jeweils ein Gehalt hinzu, die
		 * Attributwerte denken Sie sich aus. Geben Sie cap4 und cap5 auch auf dem
		 * Bildschirm aus.
		 */

		// Bildschirmausgabe
		System.out.println("Name: " + cap1.getName());
		System.out.println("Vorname: " + cap1.getSurname());
		System.out.println("Kapit�n seit: " + cap1.getCaptainYears());
		System.out.println("Gehalt: " + cap1.getSalary() + " F�derationsdukaten");
		System.out.println("Vollname: " + cap1.vollname());
		System.out.println(cap1 + "\n"); // Die toString() Methode wird aufgerufen
		
		System.out.println("\nName: " + cap2.getSurname());
		System.out.println("Vorname: " + cap2.getName());
		System.out.println("Kapit�n seit: " + cap2.getCaptainYears());
		System.out.println("Gehalt: " + cap2.getSalary() + " Darsek");
		System.out.println("Vollname: " + cap2.vollname());
		System.out.println(cap2 + "\n"); // Die toString() Methode wird aufgerufen
		
		System.out.println("\nName: " + cap3.getName());
		System.out.println("Vorname: " + cap3.getSurname());
		System.out.println("Kapit�n seit: " + cap3.getCaptainYears());
		System.out.println("Gehalt: " + cap3.getSalary() + " Kronen");
		System.out.println("Vollname: " + cap3.vollname());
		System.out.println(cap3 + "\n"); //Hierbei wird die ToString Methode aufgerufen

		// TODO: Ausgabe von cap4, erg�nzen Sie die fehlenden Methodenaufrufen
		System.out.println("Name: " + cap4.getName());
		System.out.println("Vorname: " + cap4.getSurname());
		System.out.println("Kapit�n seit: " + cap4.getCaptainYears());
		System.out.println("Gehalt: " + cap4.getSalary() + " F�derationsdukaten");
		System.out.println("Vollname: " + cap4.vollname());
		System.out.println(cap4 + "\n"); // Die toString() Methode wird aufgerufen

		// TODO: Ausgabe von cap5, erg�nzen Sie die fehlenden Methodenaufrufen
		System.out.println("Name: " + cap5.getName());
		System.out.println("Vorname: " + cap5.getSurname());
		System.out.println("Kapit�n seit: " + cap5.getCaptainYears());
		System.out.println("Gehalt: " + cap5.getSalary() + " F�derationsdukaten");
		System.out.println("Vollname: " + cap5.vollname());
		System.out.println(cap5 + "\n"); // Die toString() Methode wird aufgerufen
		
		System.out.println("Name: " + cap6.getName());
		System.out.println("Vorname: " + cap6.getSurname());
		System.out.println("Kapit�n seit: " + cap6.getCaptainYears());
		System.out.println("Gehalt: " + cap6.getSalary() + " F�derationsdukaten");
		System.out.println("Vollname: " + cap6.vollname());
		System.out.println(cap6 + "\n"); // Die toString() Methode wird aufgerufen
	}
}