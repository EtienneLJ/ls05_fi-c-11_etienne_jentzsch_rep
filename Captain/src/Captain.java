
public class Captain {

	// Attribute
	private String name;
	private String surname;
	private int captainYears;
	private double gehalt;
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden. +

	// Konstruktoren
	 
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.+
	public Captain(String name, String surname, int captainYears, double gehalt) {
		this(surname,name);
		setCaptainYears(captainYears);
		setSalary(gehalt);
	}

	// Verwaltungsmethoden
	
	public int CaptainYears() {
	return captainYears;	
	}
	
	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode f�r
	// captainYears.
	// Ber�cksichtigen Sie, dass das Jahr groe�er als 2200 sein soll.
	private void setCaptainYears(int captainYears) {
		if (captainYears <=2200)
			System.out.println("Das Jahr muss nach Christus sein");
		else
			this.captainYears = captainYears;		
	}
	public int getCaptainYears() { //neuer Konstruktor
		return this.captainYears;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	// TODO: 4. Implementieren Sie einen Konstruktor, der die Attribute surname
	// und name initialisiert. +
	public Captain(String name, String surname) {
		this.surname = surname;
		this.name = name;
	}
	
	public Captain() {
		this.surname = null;
		this.name = null;
		this.captainYears = 0;
		this.gehalt = 0.0;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSalary(double gehalt) {
		// TODO: 1. Implementieren Sie die entsprechende set-Methode.
		// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf. +
		if(gehalt > 0) 
			this.gehalt = gehalt;
		else {
			System.out.println("Das Gehalt kann nicht negativ sein und wird damit auf 0 gesetzt");
			this.gehalt = 0;
		}
	}

	public double getSalary() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methode. +
		return this.gehalt;   //0.0;
	}


	// Weitere Methoden
	
	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.
	public String vollname() {
		return this.name + " " + this.surname;
		
	}
	// TODO: 8. Implementieren Sie die toString() Methode.
	public String toString() {  // overriding the toString() method
		return name + " " + surname + " " + captainYears + " " + gehalt;
	}

}