package konsolenaufgabe2;

import java.util.Locale;

public class Temperaturtabelle {

	public static void main(String[] args) {
		String s = "Fahrenheit"; // �berschriften 
		String e = "Celsius";
		String f = "------------------------"; //22 Zeichen f�r die �berschriftslinie  + 2 safety ones
		
		int a = 20; // Ganze-Zahlen f�r Fahrenheit
		int b = 10;
		int c = 0;
		int d = 30;
		
		double v = -28.8889; // Gleitommazahlen f�r Celsius
		double w = -23.3333;
		double x = -17.7778; 
		double y = -6.6667;
		double z = -1.1111;
		
		//DEUTSCHE VARIANTE MIT KOMMA-Ausgabe anstelle von PUNKT --> double v = -28.8889; --> -28,89
		//System.out.printf( "%-12s| %10s\t\n", s, e);
		//System.out.printf("%22s\t\n", f);
		//System.out.printf( "%-12d| %10.2f\t\n" , -a, v);
		//System.out.printf( "%-12d| %10.2f\t\n" , -b, w);
		//System.out.printf( "%+-12d| %10.2f\t\n" , c, x);  
		//System.out.printf( "%+-12d| %10.2f\t\n" , a, y);  
		//System.out.printf( "%+-12d| %10.2f\t\n" , d, z); 

		//AMERIKANISCHE VARIANTE damit der Punkt f�r die DOUBLE-Aufrundung ausgegeben wird. double v = -28.8889; --> -28.89
		System.out.printf( "%-12s| %10s\t\n", s, e);
		System.out.printf("%22s\t\n", f);
		System.out.printf(Locale.US, "%-12d| %10.2f\t\n" , -a, v); // Minus f�r Linksb�ndig
		System.out.printf(Locale.US, "%-12d| %10.2f\t\n" , -b, w); // -b f�gt ein negatives Vorzeichen hinzu
		System.out.printf(Locale.US, "%+-12d| %10.2f\t\n" , c, x); // Plus und Minus f�gt der Ausgabe ein Plusvorzeichen zu
		System.out.printf(Locale.US, "%+-12d| %10.2f\t\n" , a, y); // /t = Horizontaler Tab, /n = Line Feed
		System.out.printf(Locale.US, "%+-12d| %10.2f\t\n" , d, z); // 12 und 10 geben die Abst�nde an (links und rechtsb�ndig)
		
		// Fahrenheit linksb�ndig und Celsius rechtsb�ndig!!
	}

}
