package konsolenaufgabe2;

public class Aufgabe2 {

	public static void main(String[] args) {
        double a = 0;
        double b = 1;
        double c = 2;
        double d = 3;
        double e = 4;
        double f = 5;
        double g = 6;
        double h = 24;
        double i = 120;
        
        String x = "!";
        String y = "=";
        String z = "*";
        System.out.printf( "%-1.1s", a);
        System.out.printf( "%-4s", x );
        System.out.printf( "%-2.3s", y);
        System.out.printf( "%20.1s", y );
        System.out.printf( "%4.1s\n", b);
        
        System.out.printf( "%-1.1s", b);
        System.out.printf( "%-4s", x );
        System.out.printf( "%-2.3s", y);
        System.out.printf( "%-2.1s", b);
        System.out.printf( "%18.1s", y );
        System.out.printf( "%4.1s\n", b);
        
        System.out.printf( "%-1.1s", c);
        System.out.printf( "%-4s", x );
        System.out.printf( "%-2.3s", y);
        System.out.printf( "%-2.1s", b);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", c);
        System.out.printf( "%14.1s", y );
        System.out.printf( "%4.1s\n", c);

        System.out.printf( "%-1.1s", d);
        System.out.printf( "%-4s", x );
        System.out.printf( "%-2.3s", y);
        System.out.printf( "%-2.1s", b);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", c);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", d);
        System.out.printf( "%10.1s", y );
        System.out.printf( "%4.1s\n", g);
        
        System.out.printf( "%-1.1s", e);
        System.out.printf( "%-4s", x );
        System.out.printf( "%-2.3s", y);
        System.out.printf( "%-2.1s", b);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", c);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", d);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", e);
        System.out.printf( "%6.1s", y );
        System.out.printf( "%4.2s\n", h);
        
        
        System.out.printf( "%-1.1s", f);
        System.out.printf( "%-4s", x );
        System.out.printf( "%-2.3s", y);
        System.out.printf( "%-2.1s", b);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", c);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", d);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", e);
        System.out.printf( "%-2.1s", z );
        System.out.printf( "%-2.1s", f);
        System.out.printf( "%2.1s", y );
        System.out.printf( "%4.3s", i);
        
	}

}
