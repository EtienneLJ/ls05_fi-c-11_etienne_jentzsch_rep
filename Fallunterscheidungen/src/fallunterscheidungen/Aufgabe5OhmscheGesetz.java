package fallunterscheidungen;
import java.util.Scanner;
public class Aufgabe5OhmscheGesetz {

	public static void main(String[] args) {
			char elektrik;
			double u, i, r; //Spannung;Strom;Widerstand
			Scanner MyScanner = new Scanner(System.in);
			System.out.println("Was moechten Sie berechnen? Spannung; Strom; Widerstand (U, I, R): ");
			elektrik = MyScanner.next().charAt(0);
				
			switch (elektrik) {
				case 'R': //Gro�- und Kleinschreibung beachten
				case 'r':
				System.out.println("Spannung in Volt eingeben: "); // Eingabe
				u = MyScanner.nextDouble();
				System.out.println("Strom in Ampere eingeben: "); // Eingabe
				i = MyScanner.nextDouble();
				r = u / i;  // Verarbeitung
				System.out.printf("R = %.2f Ohm", r); // Ausgabe
				break;
				case 'U':
				case 'u':
				System.out.println("Strom in Ampere eingeben: "); // Eingabe
				i = MyScanner.nextDouble();
				System.out.println("Widerstand in Ohm eingeben: "); // Eingabe
				r = MyScanner.nextDouble();
				u = i * r;  // Verarbeitung
				System.out.printf("U = %.2f Volt", u); // Ausgabe
				break;
				case 'I':
				case 'i':
				System.out.println("Spannung in Volt eingeben: "); // Eingabe
				u = MyScanner.nextDouble();
				System.out.println("Widerstand in Ohm eingeben: "); // Eingabe
				r = MyScanner.nextDouble();
				i = u / r; // Verarbeitung
				System.out.printf("I = %.2f Ampere", i); // Ausgabe
				break;
				default:
				System.out.println("Falsche Eingabe!");
				break;
				}
				MyScanner.close(); // Scanner schlie�en
			}

		}
