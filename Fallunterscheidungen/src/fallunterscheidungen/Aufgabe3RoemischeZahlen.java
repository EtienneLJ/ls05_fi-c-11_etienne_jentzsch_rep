package fallunterscheidungen;
import java.util.Scanner;
public class Aufgabe3RoemischeZahlen {

	public static void main(String[] args) {		    
		    char eingabeRoemZahl; // Eingabe der R�mischen Zahl
		    Scanner MyScanner = new Scanner(System.in);    // Scanner f�r die Abfrage gebaut
		    System.out.print("Bitte geben Sie ein r�misches Zahl an: "); //Scanner Nachricht an den Benutzer
		    eingabeRoemZahl = MyScanner.next().charAt(0); // Variable geschaffen um die Eingabe zu speichern
	 
		    switch (eingabeRoemZahl) { // Alle F�lle aufgelsitet f�r die Eingabe der r�msichen Zahl (Noch keine Verkn�pfung gebaut)
		      case 'I':  
		      System.out.println("Wert: 1");
		      break;
		      case 'V':
		      System.out.println("Wert: 5");
		      break;
		      case 'X':
		      System.out.println("Wert: 10");
		      break;
		      case 'L':
		      System.out.println("Wert: 50");
		      break;
		      case 'C':
		      System.out.println("Wert: 100");
		      break;
		      case 'D':
		      System.out.println("Wert: 500");
		      break;
		      case 'M':
		      System.out.println("Wert: 1000");
		      break;
		      default:
		      System.out.println(eingabeRoemZahl + " ist keine r�mische Zahl.");
		      break; 
		    } 
		    MyScanner.close();
		  } 
	
		}
